#!/bin/bash

set -eu

if ! [ -e "/app/data/data" ]; then
    echo "Initializing data directory on first run"
    cp -r /app/code/data/. /app/data/data
else
    # this allows deleting playground, formatting pages and not bring them back on restart
    rsync -v -a --ignore-existing --exclude "pages/*" --exclude "log/" /app/code/data/ /app/data/data/
fi

mkdir -p /app/data/conf /app/data/templates /app/data/plugins /run/dokuwiki/sessions /run/dokuwiki/log
rm -rf /app/data/data/log && ln -s /run/dokuwiki/log /app/data/data/log

# Create 'regular' conf files so that they are immediately writable
# dist, example, template files are not copied. they have to copied over 'carefully'
cp /app/code/conf.orig/.htaccess /app/code/conf.orig/*.php /app/code/conf.orig/*.conf /app/data/conf/

# required for siteexport plugin
if [[ ! -f "/app/data/preload.php" ]]; then
    cp /app/code/inc/preload.php.dist /app/data/preload.php
fi

for f in $(ls /app/code/lib/tpl.orig); do
    rm -rf "/app/data/templates/$f"
    cp -rf "/app/code/lib/tpl.orig/$f" "/app/data/templates/$f"
done

for f in $(ls /app/code/lib/plugins.orig); do
    rm -rf "/app/data/plugins/$f"
    # skip copying auth plugins (see #10)
    if [[ ! -d "/app/code/lib/plugins.orig/$f" || $f != auth* ]]; then
        echo "Copying $f"
        cp -rf "/app/code/lib/plugins.orig/$f" "/app/data/plugins/$f"
    fi
done

[[ -z "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-}" ]] && export CLOUDRON_MAIL_FROM_DISPLAY_NAME=DokuWiki
[[ -z "${CLOUDRON_OIDC_PROVIDER_NAME:-}" ]] && export CLOUDRON_OIDC_PROVIDER_NAME=Cloudron

# https://www.dokuwiki.org/plugin:config#protecting_settings
cp /app/code/conf.orig/local.protected.php.template /app/data/conf/local.protected.php

# we only provide a template, the user can change this using the acl UI
# this file is needed for SSO and non-SSO modes
if [[ ! -f "/app/data/conf/acl.auth.php" ]]; then
    cp /app/code/conf.orig/acl.auth.php.template /app/data/conf/acl.auth.php
fi

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "Setting up OIDC"

    cp -rf "/app/code/lib/plugins.orig/authplain" "/app/data/plugins/"
    cp -rf "/app/code/lib/plugins.orig/oauth" "/app/data/plugins/"
    cp -rf "/app/code/lib/plugins.orig/oauthgeneric" "/app/data/plugins/"

    # putting this in protected file ensures user cannot change it in UI
    if ! grep -q "plugins\['oauth'\] = 1" /app/data/conf/plugins.required.php; then
        echo -e "\n\$plugins['oauth'] = 1;\n\$plugins['oauthgeneric'] = 1;\n" >> /app/data/conf/plugins.required.php
    fi

    if [[ ! -f /app/data/conf/users.auth.php ]]; then
        cp /app/code/conf.orig/users.auth.php.dist /app/data/conf/users.auth.php
    fi

    # be careful as to what is in this file since not all values are persisted by the admin UI
    if [[ ! -f /app/data/conf/local.php ]]; then
        cat > /app/data/conf/local.php <<'EOF'
<?php

// Add custom configuration here
// make Cloudron users as doku wiki admins (https://www.dokuwiki.org/config:superuser)
// $conf['superuser']   = 'username';
EOF
    fi

    # previous LDAP plugin had registration disabled by default
    if ! grep -q openregister /app/data/conf/local.php; then
        echo -e "\n\$conf['openregister'] = 0;\n" >> /app/data/conf/local.php
    fi
else
    echo "Setting up plain auth"

    cp -rf "/app/code/lib/plugins.orig/authplain" "/app/data/plugins/authplain"

    if [[ ! -f /app/data/conf/users.auth.php ]]; then
        cp /app/code/conf.orig/users.auth.php.dist /app/data/conf/users.auth.php
    fi

    if [[ ! -f /app/data/conf/local.php ]]; then
        cat > /app/data/conf/local.php <<'EOF'
<?php

// Add custom configuration here
// $conf['title'] = 'My Wiki';
EOF
    fi
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

chown -R www-data:www-data /app/data /run/dokuwiki

echo "Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
