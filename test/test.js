#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until, Key} = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 5000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TIMEOUT);
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function login(user, pass) {
        await browser.get('about:blank');
        await browser.sleep(2000);
        await browser.get(`https://${app.fqdn}/start?do=login`);
        await waitForElement(By.xpath('//input[@name="u"]'));
        await browser.findElement(By.xpath('//input[@name="u"]')).sendKeys(user);
        await browser.findElement(By.xpath('//input[@type="password"]')).sendKeys(pass);
        await browser.findElement(By.xpath('//input[@type="password"]')).sendKeys(Key.RETURN); // form submit does not work
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === `https://${app.fqdn}` + '/start';
        }, 400000);
    }

    async function loginOIDC(username, password, alreadyAuthenticated) {
        await browser.manage().deleteAllCookies();

        await browser.get('https://' + app.fqdn + '/start?do=login');
        await sleep(2000);

        await waitForElement(By.xpath('//a[@class="plugin_oauth_generic"]'));
        await browser.findElement(By.xpath('//a[@class="plugin_oauth_generic"]')).click();
        await browser.sleep(3000);

        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }
        await waitForElement(By.xpath('//a[contains(@href, "logout")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/start?do=profile`);
        await waitForElement(By.xpath('//a[contains(@href, "logout")]'));
        await browser.findElement(By.xpath('//a[contains(@href, "logout")]')).click();

        await waitForElement(By.xpath('//input[@name="u"]'));
    }

    async function uploadImage() {
        await browser.get(`https://${app.fqdn}/start&do=media&ns=`);
        await browser.findElement(By.xpath('//a[text()="Upload"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//input[@type="file" and @name="file"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@type="file" and @name="file"]')).sendKeys(path.resolve(__dirname, '../logo.png'));
        await browser.findElement(By.xpath('//button[@id="mediamanager__upload_button"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//a[text()="logo.png"]')), TIMEOUT);
    }

    async function editPage() {
        await browser.get(`https://${app.fqdn}/start&do=edit`);
        await browser.wait(until.elementLocated(By.xpath('//textarea')), TIMEOUT);
        await browser.findElement(By.xpath('//textarea')).sendKeys(Key.chord(Key.CONTROL, 'a'));
        await browser.findElement(By.xpath('//textarea')).sendKeys('hello cloudron');
        const button = browser.findElement(By.xpath('//button[text()="Save"]'));
        await browser.executeScript('arguments[0].scrollIntoView(false)', button);
        await browser.findElement(By.xpath('//button[text()="Save"]')).click();
        await browser.wait(async function () {
            const url = await browser.getCurrentUrl();
            return url === `https://${app.fqdn}/start`;
        }, TIMEOUT);
    }

    async function viewExistingPage() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//*[contains(text(), "hello cloudron")]')), TIMEOUT);
    }

    async function isLoggedIn() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//*[contains(text(), "Logged in as:")]')), TIMEOUT);
    }

    async function isLoggedInAsAdmin() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//a[@title="Admin"]')), TIMEOUT);
    }

    async function viewUploadedImage() {
        await browser.get(`https://${app.fqdn}` + '/lib/exe/fetch.php?media=logo.png');
        await browser.wait(until.elementLocated(By.xpath('//img[contains(@src, "logo.png")]')), TIMEOUT);
    }

    async function viewExistingImage() {
        await browser.get(`https://${app.fqdn}` + '/lib/exe/fetch.php?media=logo.png');
        await browser.wait(until.elementLocated(By.xpath('//img[contains(@src, "logo.png")]')), TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // non sso
    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can see register link', async function () {
        await browser.get(`https://${app.fqdn}` + '/');
        await waitForElement(By.xpath('//a[@title="Register"]'));
    });

    // this add user 'girish' and password 'bakwulnia70!'
    it('add a user', async function () {
        fs.writeFileSync('/tmp/users.auth.php', 'girish:$1$DqpFHovR$xlcVojXVsE6dpNPCVYt/o.:Girish:mail@girish.in:user\n');
        execSync(`cloudron push --app ${app.id} /tmp/users.auth.php /app/data/conf/users.auth.php`);
        fs.writeFileSync('/tmp/local.php', '<?php\n\n$conf["superuser"] = ("girish");\n');
        execSync(`cloudron push --app ${app.id} /tmp/local.php /app/data/conf/local.php`);
    });

    it('can login', login.bind(null, 'girish', 'bakwulnia70!'));
    it('logged into wiki', isLoggedIn);
    it('logged user as admin', isLoggedInAsAdmin);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // sso mode
    it('install app (sso)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can get the main page', async function () {
        await browser.get(`https://${app.fqdn}`);
    });

    it('make user an admin', function () {
        fs.writeFileSync('/tmp/local.php', `<?php\n$conf['superuser']   = '${username}';\n`);
        execSync(`cloudron push --app ${app.id} /tmp/local.php /app/data/conf/local.php`);
    });

    it('can login via OIDC', loginOIDC.bind(null, username, password, false));

    it('logged into wiki', isLoggedIn);
    it('logged user as admin', isLoggedInAsAdmin);
    it('can edit page', editPage);
    it('can upload image', uploadImage);
    it('can logout', logout);

    it('can restart app', function () { execSync('cloudron restart', EXEC_ARGS); });

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can see existing page', viewExistingPage);
    it('can see uploaded image', viewExistingImage);
    it('can logout', logout);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('can see existing page', viewExistingPage);
    it('can see uploaded image', viewExistingImage);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', EXEC_ARGS);
        getAppInfo();
        expect(app).to.be.an('object');
    });

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('logged user as admin', isLoggedInAsAdmin);
    it('can see existing page', viewExistingPage);
    it('can see uploaded image', viewUploadedImage);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', function () { execSync('cloudron install --appstore-id org.dokuwiki.cloudronapp --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('make user an admin', function () {
        fs.writeFileSync('/tmp/local.php', `<?php\n$conf['superuser']   = '${username}';\n`);
        execSync(`cloudron push --app ${app.id} /tmp/local.php /app/data/conf/local.php`);
    });
    it('can login', loginOIDC.bind(null, username, password, true));
    it('logged into wiki', isLoggedIn);
    it('logged user as admin', isLoggedInAsAdmin);
    it('can edit page', editPage);
    it('can upload a file', uploadImage);
    it('can logout', logout);

    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });

    it('can login via OIDC', loginOIDC.bind(null, username, password, true));
    it('logged user as admin', isLoggedInAsAdmin);
    it('can see existing page', viewExistingPage);
    it('can see uploaded image', viewUploadedImage);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
