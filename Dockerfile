FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=dokuwiki/dokuwiki versioning=loose extractVersion=^release-(?<version>.+)$
ARG DOKWIKI_VERSION=2024-02-06b
RUN curl -L http://download.dokuwiki.org/src/dokuwiki/dokuwiki-${DOKWIKI_VERSION}.tgz | tar -xz --strip-components 1 -f - && \
    rm /app/code/install.php

ADD conf /app/code/conf
ADD htaccess /app/code/.htaccess

# renovate: datasource=github-tags depName=cosmocode/dokuwiki-plugin-oauth versioning=loose
ARG OAUTH_VERSION=2024-11-02
RUN mkdir /app/code/lib/plugins/oauth && \
    curl -L https://github.com/cosmocode/dokuwiki-plugin-oauth/archive/refs/tags/${OAUTH_VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code/lib/plugins/oauth/ -f -

# renovate: datasource=github-tags depName=cosmocode/dokuwiki-plugin-oauthgeneric versioning=loose
ARG OAUTH_GENERIC_VERSION=2024-03-21
RUN mkdir /app/code/lib/plugins/oauthgeneric && \
    curl -L https://github.com/cosmocode/dokuwiki-plugin-oauthgeneric/archive/refs/tags/${OAUTH_GENERIC_VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code/lib/plugins/oauthgeneric/ -f -

# renovate: datasource=github-tags depName=splitbrain/dokuwiki-plugin-smtp versioning=loose
ARG SMTP_VERSION=2023-04-03
RUN mkdir /app/code/lib/plugins/smtp && \
    curl -L https://github.com/splitbrain/dokuwiki-plugin-smtp/archive/refs/tags/${SMTP_VERSION}.tar.gz | tar -xz --strip-components 1 -C /app/code/lib/plugins/smtp/ -f -

# this is not in plugins.protected.php because dokuwiki shows a 'reinstall' button in the extension manager
RUN echo -e "\n\$plugins['smtp'] = 1;\n" >> /app/code/conf/plugins.required.php

# https://www.dokuwiki.org/config
RUN mv /app/code/conf /app/code/conf.orig && \
    mv /app/code/lib/tpl /app/code/lib/tpl.orig && \
    mv /app/code/lib/plugins /app/code/lib/plugins.orig && \
    ln -s /app/data/templates /app/code/lib/tpl && \
    ln -s /app/data/plugins /app/code/lib/plugins && \
    ln -s /app/data/conf /app/code/conf && \
    ln -s /app/data/preload.php /app/code/inc/preload.php

RUN chown -R www-data.www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/dokuwiki.conf /etc/apache2/sites-enabled/dokuwiki.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/dokuwiki/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

# configuring rewrite
RUN a2enmod rewrite

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
