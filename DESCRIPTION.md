DokuWiki is a simple to use and highly versatile Open Source wiki software.

It is loved by users for its clean and readable syntax. Wikis are quick to
update and new pages are easily added. Designed for collaboration while 
maintaining a history of every change, DokuWiki could be used as

* Corporate Knowledge Base
* Private notebook
* Software manual
* Project workspace
* CMS – intranet

#### Accounts

This app allows edits by any Cloudron user. Cloudron administrators are automatically
made wiki administrators.

#### Changing the Logo

To change the logo upload a file named `logo.png` at the root of the wiki.

#### Links

* [Submit bugs and feature wishes](https://github.com/splitbrain/dokuwiki/issues)
* [Forum](http://forum.dokuwiki.org/)

